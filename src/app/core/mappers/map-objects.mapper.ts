import {MapObjectResponse} from "../../feature/map/models/map-object.response";
import {MapObject} from "../../feature/map/models/map-object.model";

export class MapObjectsMapper {
  public static fromResponseSingle(mapObjectResponse: MapObjectResponse): MapObject {
    return {
      id: mapObjectResponse.id.toString(),
      name: mapObjectResponse.name,
      description: mapObjectResponse.description,
      city: mapObjectResponse.city,
      street: mapObjectResponse.street,
      buildingNumber: mapObjectResponse.buildingNumber,
      imageUrl: mapObjectResponse.imageUrl,
      coordinates: {
        latitude: mapObjectResponse.coordinates[0],
        longitude: mapObjectResponse.coordinates[1]
      },
      objectType: {
        id: mapObjectResponse.objectType.id.toString(),
        name: mapObjectResponse.objectType.name
      }
    }
  }

  public static fromResponseMultiple(mapObjectsResponse: MapObjectResponse[]): MapObject[] {
    return mapObjectsResponse.map(mapObject => MapObjectsMapper.fromResponseSingle(mapObject));
  }
}
