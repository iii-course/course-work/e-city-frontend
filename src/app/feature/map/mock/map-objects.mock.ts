import {MapObject} from "../models/map-object.model";

export const mockMapObjects: MapObject[] = [
  {
    id: '23434bebre23',
    name: 'St. Paul\'s Cathedral',
    description: 'Churchyard and gardens outside Saint Paul\'s cathedral, with a floor-plan of the original building.',
    imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/c/cb/St_Pauls_aerial_%28cropped%29.jpg',
    coordinates: {
      latitude: 51.514,
      longitude: -0.096
    },
    street: 'St. Paul\'s Churchyard',
    city: 'London'
  },
  {
    id: '23234S4bEDre23',
    name: 'Bank of England Museum',
    description: 'Family-friendly museum with interactive exhibitions telling of the Bank\'s role in today\'s economy.',
    imageUrl: 'https://d3d00swyhr67nd.cloudfront.net/_source/COL_BOE_location_image_1.jpg',
    coordinates: {
      latitude: 51.515,
      longitude:  -0.086
    },
    street: 'Bartholomew Ln',
    city: 'London'
  },
  {
    id: '34FD3434bebre23',
    name: 'Tower of London',
    description: 'Centuries of bloody history around a medieval castle, home to Crown Jewels and iconic Beefeaters.',
    imageUrl: 'https://aws-tiqets-cdn.imgix.net/images/content/65144ff87b2a4c2b9e73643b80c4f876.jpg?auto=format&fit=crop&ixlib=python-3.2.1&q=70&s=f863922e5812cb2d0c10b5dc92a5f704',
    coordinates: {
      latitude: 51.509,
      longitude: -0.075
    },
    street: 'The Tower of London',
    city: 'London',
    buildingNumber: 12,
  },
  {
    id: '34202334bebre23',
    name: 'The Gym Group London Monument\n',
    description: 'We’re the UK’s best value 24/7 gym with over 207 locations nationwide and counting! It’s our mission to break down barriers to fitness for everyone in the UK, providing top-quality gyms and expert advice at affordable prices.',
    imageUrl: 'https://lh5.googleusercontent.com/p/AF1QipPNaiMKBjMiCOFpHRZHRfiuqVS_uAoqUc91OiKT=w426-h240-k-no',
    coordinates: {
      latitude: 51.5092,
      longitude: -0.0841
    },
    street: 'Lower Thames St',
    city: 'London',
    buildingNumber: 10,
  },
  {
    id: '00D3434bebre23',
    name: 'Premier Inn London Bank (Tower) hotel',
    description: 'Our award-winning family hotel in the City of London puts you and your family within easy walking distance of some of the city\'s most famous historic attractions, including the iconic Tower of London and Tower Bridge. If you\'re here for work, you\'re well placed for key destinations within the Square Mile and financial district. Or, if ghost stories are more your thing, explore the gothic history of the Tower of London, home to the Crown Jewels and the famous Beefeaters. No visit is complete without a stroll along the River Thames to take in the beauty of St Paul\'s Cathedral. And at the end of a great day exploring, enjoy a relaxing meal in our family-friendly restaurant. A great night\'s sleep awaits...',
    imageUrl: 'https://lh5.googleusercontent.com/p/AF1QipO_PaqgDWVVjb6SvjcXECcn8F7z_lZ7C77xCs50=w408-h272-k-no',
    coordinates: {
      latitude: 51.5097,
      longitude: -0.083
    },
    street: 'St Mary at Hill',
    city: 'London',
    buildingNumber: 20,
  },
];
