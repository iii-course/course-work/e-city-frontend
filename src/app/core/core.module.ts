import {NgModule, Optional, SkipSelf} from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import {MatToolbarModule} from "@angular/material/toolbar";
import {TranslateModule} from "@ngx-translate/core";
import {MatButtonModule} from "@angular/material/button";
import {HttpClientModule} from "@angular/common/http";
import {DRAWER_DATA, DrawerData} from "./tokens/drawer-data.token";
import {RouterModule} from "@angular/router";
import {MatIconModule} from "@angular/material/icon";

@NgModule({
  declarations: [
    HeaderComponent
  ],
    imports: [
        CommonModule,
        HttpClientModule,
        TranslateModule,
        MatToolbarModule,
        MatButtonModule,
        RouterModule,
        MatIconModule
    ],
  exports: [
    HeaderComponent
  ],
  providers: [{ provide: DRAWER_DATA, useClass: DrawerData }]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule?: CoreModule) {
    if (parentModule) {
      throw new Error('CoreModule must be imported only once.');
    }
  }
}
