import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReviewCardComponent } from './components/review-card/review-card.component';
import { ReviewsListComponent } from './components/reviews-list/reviews-list.component';
import {TranslateModule} from "@ngx-translate/core";
import {MatCardModule} from "@angular/material/card";
import {MatIconModule} from "@angular/material/icon";


@NgModule({
  declarations: [
    ReviewCardComponent,
    ReviewsListComponent
  ],
  imports: [
    CommonModule,
    TranslateModule,
    MatCardModule,
    MatIconModule
  ],
  exports: [
    ReviewsListComponent
  ]
})
export class ReviewModule { }
