import { Component, OnInit } from '@angular/core';
import {User} from "../../../../core/models/user/user.model";
import {UserService} from "../../../../core/services/user/user.service";
import {Observable, of} from "rxjs";
import {Review} from "../../../../core/models/review/review.model";
import {MapObject} from "../../../map/models/map-object.model";
import {Route, Router} from "@angular/router";

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss']
})
export class ProfilePageComponent {
  public readonly user: User | undefined;
  public readonly reviews$: Observable<Review[]>;
  public readonly favoriteObjects$: Observable<MapObject[]>;

  constructor(
    private readonly router: Router,
    userService: UserService
  ) {
    this.user = userService.getUser();
    this.reviews$ = userService.getAllReviews();
    this.favoriteObjects$ = userService.getAllFavouriteObjects();
  }

  public getUserProfileImage(): string {
    return this.user?.imageUrl ?? './assets/images/profile_image.webp';
  }

  public goToSettings(): void {
    this.router.navigateByUrl('profile/settings');
  }
}
