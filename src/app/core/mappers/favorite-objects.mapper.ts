import {ReviewResponse} from "../models/review/review-response.model";
import {Review} from "../models/review/review.model";
import {MapObjectsMapper} from "./map-objects.mapper";
import {UsersMapper} from "./users.mapper";
import {FavoriteObjectResponse} from "../models/favorite/favorite-object-response.model";
import {MapObject} from "../../feature/map/models/map-object.model";

export class FavoriteObjectsMapper {
  public static fromResponseSingle(favouriteObjectResponse: FavoriteObjectResponse): MapObject {
    return MapObjectsMapper.fromResponseSingle(favouriteObjectResponse.object);
  }

  public static fromResponseMultiple(reviewsResponse: FavoriteObjectResponse[]): MapObject[] {
    return reviewsResponse.map(reviewResponse => FavoriteObjectsMapper.fromResponseSingle(reviewResponse));
  }
}
