import {Injectable, InjectionToken} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DrawerData {
  private _data: any;

  public get<T>(): T {
    return this._data as T;
  }

  public set<T>(data: T): void {
    this._data = data;
  }
}

export const DRAWER_DATA = new InjectionToken<DrawerData>('Data for the main drawer');
