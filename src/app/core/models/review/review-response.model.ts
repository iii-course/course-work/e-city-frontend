import {MapObjectResponse} from "../../../feature/map/models/map-object.response";
import {UserResponse} from "../user/user-response.model";

export interface ReviewResponse {
  id: number,
  object: MapObjectResponse,
  rate: number,
  heading: string,
  text: string,
  author: UserResponse
}
