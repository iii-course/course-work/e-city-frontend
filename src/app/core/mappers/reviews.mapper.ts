import {ReviewResponse} from "../models/review/review-response.model";
import {Review} from "../models/review/review.model";
import {MapObjectsMapper} from "./map-objects.mapper";
import {UsersMapper} from "./users.mapper";

export class ReviewsMapper {
  public static fromResponseSingle(reviewResponse: ReviewResponse): Review {
    return {
      id: reviewResponse.id.toString(),
      object: MapObjectsMapper.fromResponseSingle(reviewResponse.object),
      rate: reviewResponse.rate,
      heading: reviewResponse.heading,
      text: reviewResponse.text,
      author: UsersMapper.fromResponse(reviewResponse.author)
    };
  }

  public static fromResponseMultiple(reviewsResponse: ReviewResponse[]): Review[] {
    return reviewsResponse.map(reviewResponse => ReviewsMapper.fromResponseSingle(reviewResponse));
  }
}
