export interface Coordinates {
  readonly latitude: number;
  readonly longitude: number;
}
