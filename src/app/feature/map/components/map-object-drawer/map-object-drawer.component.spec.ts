import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapObjectDrawerComponent } from './map-object-drawer.component';

describe('MapObjectDrawerComponent', () => {
  let component: MapObjectDrawerComponent;
  let fixture: ComponentFixture<MapObjectDrawerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapObjectDrawerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapObjectDrawerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
