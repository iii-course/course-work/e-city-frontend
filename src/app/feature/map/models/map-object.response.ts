export interface MapObjectResponse {
  readonly id: number,
  readonly name: string,
  readonly description: string,
  readonly city: string,
  readonly street: string;
  readonly buildingNumber?: number;
  readonly imageUrl: string,
  readonly coordinates: [number, number],
  readonly objectType: {
    id: number,
    name: string
  }
}
