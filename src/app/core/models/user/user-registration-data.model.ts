export interface UserRegistrationData {
  readonly firstName: string,
  readonly lastName: string,
  readonly email: string,
  readonly password: string
}
