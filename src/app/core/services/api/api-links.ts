export const apiLinks = {
  // Auth
  signUp: '/users',
  logIn: '/users',

  // Map Objects
  getAllObjects: '/map-objects',

  // User
  getAllUsers: '/users',
  getAllUserReviews: '/users/:id/reviews'
};
