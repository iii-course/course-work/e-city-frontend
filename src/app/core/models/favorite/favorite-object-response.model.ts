import {MapObjectResponse} from "../../../feature/map/models/map-object.response";

export interface FavoriteObjectResponse {
  object: MapObjectResponse;
}
