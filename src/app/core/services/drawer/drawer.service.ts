import {
  Component,
  ComponentFactory,
  ComponentFactoryResolver,
  ComponentRef,
  Inject,
  Injectable,
  Type
} from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {shareReplay} from "rxjs/operators";
import {DRAWER_DATA, DrawerData} from "../../tokens/drawer-data.token";

export interface DrawerConfig {
  component: Type<any>;
  data: any
}

@Injectable({
  providedIn: 'root'
})
export class DrawerService {
  private readonly drawer = new BehaviorSubject<DrawerConfig | undefined>(undefined);

  public readonly drawer$ = this.drawer.asObservable().pipe(
    shareReplay({ bufferSize: 1, refCount: true })
  );

  public openDrawer(drawer: Type<any>, data?: any): void {
    this.drawer.next({ component: drawer, data });
  }

  public closeDrawer(): void {
    console.log('close drawer')
    this.drawer.next(undefined);
  }
}
