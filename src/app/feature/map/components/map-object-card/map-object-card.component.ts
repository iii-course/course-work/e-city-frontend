import {Component, Input} from "@angular/core";
import {MapObject} from "../../models/map-object.model";

@Component({
  selector: 'app-map-object-card[object]',
  templateUrl: './map-object-card.component.html',
  styleUrls: ['./map-object-card.component.scss']
})
export class MapObjectCardComponent {
  @Input() public object!: MapObject;

  constructor() { }

}
