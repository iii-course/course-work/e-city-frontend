import {ChangeDetectionStrategy, Component} from '@angular/core';
import {Router} from "@angular/router";
import {UserService} from "../../services/user/user.service";
import {AuthService} from "../../services/auth/auth.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent {
  public readonly isLoggedIn$ = this.userService.isLoggedIn$;

  constructor(
    private readonly router: Router,
    private readonly userService: UserService,
    private readonly authService: AuthService
  ) { }

  public logOut(): void {
    this.authService.logOut();
    this.goToLogin();
  }

  public goToLogin(): void {
    this.router.navigateByUrl('/auth/login');
  }

  public goToRegistration(): void {
    this.router.navigateByUrl('/auth/sign-up');
  }

  public goToProfile(): void {
    this.router.navigateByUrl('/profile');
  }
}
