import { Injectable } from '@angular/core';
import {ApiService} from "../api/api.service";
import {BehaviorSubject, Observable} from "rxjs";
import {UserRegistrationData} from "../../models/user/user-registration-data.model";
import {apiLinks} from "../api/api-links";
import {UserLoginData} from "../../models/user/user-login-data.model";
import {UserResponse} from "../../models/user/user-response.model";
import {User} from "../../models/user/user.model";
import {UsersMapper} from "../../mappers/users.mapper";
import {map, tap} from "rxjs/operators";
import {UserService} from "../user/user.service";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(
    private readonly apiService: ApiService,
    private readonly userService: UserService
  ) { }

  public signUp(userData: UserRegistrationData): Observable<void> {
    return this.apiService.post<void>(apiLinks.signUp, userData);
  }

  public logIn(userData: UserLoginData): Observable<User> {
    return this.apiService.get<UserResponse[]>(this.getLogInUrl(userData)).pipe(
      map(userResponse => userResponse[0]),
      tap(userResponse => {
        if (!userResponse) {
          throw new Error('No user found');
        }
      }),
      map(userResponse => UsersMapper.fromResponse(userResponse)),
      tap(user => this.userService.setUser(user))
    );
  }

  public logOut(): void {
    this.userService.clearUser();
  }

  private getLogInUrl({email, password}: UserLoginData): string {
    return `${apiLinks.logIn}?email=${email}&password=${password}`;
  }
}
