import {Environment} from "./environment.common";

export const environment: Environment = {
  production: true,
  baseUrl: 'http://localhost:3000'
};
