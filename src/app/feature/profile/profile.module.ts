import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfilePageComponent } from './pages/profile-page/profile-page.component';
import {TranslateModule} from "@ngx-translate/core";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {MatGridListModule} from "@angular/material/grid-list";
import {ReviewModule} from "../review/review.module";
import {MapModule} from "../map/map.module";
import { SettingsPageComponent } from './pages/settings-page/settings-page.component';
import {MatInputModule} from "@angular/material/input";
import {ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    ProfilePageComponent,
    SettingsPageComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ProfileRoutingModule,
    TranslateModule,
    MatButtonModule,
    MatIconModule,
    MatGridListModule,
    ReviewModule,
    MapModule,
    MatInputModule
  ]
})
export class ProfileModule { }
