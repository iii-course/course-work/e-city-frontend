import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  private storage: Storage;

  constructor() {
    this.storage = localStorage;
  }

  public getItem<T>(key: string): T | undefined {
    const item = this.storage.getItem(key);
    return item ? JSON.parse(item) : undefined;
  }

  public setItem<T>(key: string, item: T): void {
    this.storage.setItem(key, JSON.stringify(item));
  }

  public removeItem(key: string): void {
    this.storage.removeItem(key);
  }
}
