import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapObjectsListComponent } from './map-objects-list.component';

describe('MapObjectsListComponent', () => {
  let component: MapObjectsListComponent;
  let fixture: ComponentFixture<MapObjectsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapObjectsListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MapObjectsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
