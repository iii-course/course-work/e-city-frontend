import {Component, OnDestroy} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../../../core/services/auth/auth.service";
import {Subject} from "rxjs";
import {UserLoginData} from "../../../../core/models/user/user-login-data.model";
import {takeUntil} from "rxjs/operators";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnDestroy {
  private readonly destroy = new Subject();

  public readonly form = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required]),
  });

  constructor(
    private readonly authService: AuthService,
    private readonly router: Router
  ) { }

  public ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }

  public logIn(): void {
    if (this.form.valid) {
      this.authService.logIn(this.form.value as UserLoginData).pipe(
        takeUntil(this.destroy)
      ).subscribe({
        next: () => this.onLoginSuccess(),
        error: (e) => this.onLoginError(e)
      });
    }
  }

  private onLoginSuccess(): void {
    this.router.navigateByUrl('/');
  }

  private onLoginError(e: any): void {
    console.log(e)
    alert('User not found. Please try again.');
    this.form.reset();
  }
}
