import {Coordinates} from "./coordinates.model";
import {MapObjectType} from "./map-object-type.model";

export interface MapObject {
  readonly id: string,
  readonly name: string,
  readonly description: string,
  readonly city: string,
  readonly street: string;
  readonly buildingNumber?: number;
  readonly imageUrl: string,
  readonly coordinates: Coordinates,
  readonly objectType: MapObjectType
}
