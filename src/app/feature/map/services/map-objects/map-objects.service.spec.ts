import { TestBed } from '@angular/core/testing';

import { MapObjectsService } from './map-objects.service';

describe('MapObjectsService', () => {
  let service: MapObjectsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MapObjectsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
