import { Injectable } from '@angular/core';
import {LocalStorageService} from "../local-storage/local-storage.service";
import {User} from "../../models/user/user.model";
import {BehaviorSubject, Observable, of} from "rxjs";
import {ApiService} from "../api/api.service";
import {Review} from "../../models/review/review.model";
import {ReviewResponse} from "../../models/review/review-response.model";
import {apiLinks} from "../api/api-links";
import {map} from "rxjs/operators";
import {ReviewsMapper} from "../../mappers/reviews.mapper";
import {MapObject} from "../../../feature/map/models/map-object.model";
import {FavoriteObjectResponse} from "../../models/favorite/favorite-object-response.model";
import {FavoriteObjectsMapper} from "../../mappers/favorite-objects.mapper";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private readonly isLoggedIn: BehaviorSubject<boolean>;
  private readonly userKey: string;
  private user: User | undefined;

  public readonly isLoggedIn$: Observable<boolean>;

  constructor(
    private readonly localStorage: LocalStorageService,
    private readonly apiService: ApiService
  ) {
    this.userKey = 'user';
    this.isLoggedIn = new BehaviorSubject<boolean>(!!this.getUser());
    this.isLoggedIn$ = this.isLoggedIn.asObservable();
  }

  public setUser(user: User): void {
    this.user = user;
    this.localStorage.setItem<User>(this.userKey, user);
    this.isLoggedIn.next(true);
  }

  public getUser(): User | undefined {
    if (!this.user) {
      this.user = this.localStorage.getItem<User>(this.userKey);
    }
    return this.user;
  }

  public clearUser(): void {
    this.user = undefined;
    this.localStorage.removeItem(this.userKey);
    this.isLoggedIn.next(false);
  }

  public getAllReviews(): Observable<Review[]> {
    if (!this.user) {
      return of([]);
    }
    return this.apiService.get<ReviewResponse[]>(`/users/${this.user?.id}/reviews`).pipe(
      map(response => ReviewsMapper.fromResponseMultiple(response))
    );
  }

  public getAllFavouriteObjects(): Observable<MapObject[]> {
    if (!this.user) {
      return of([]);
    }
    return this.apiService.get<FavoriteObjectResponse[]>(`/users/${this.user?.id}/favorites`).pipe(
      map(response => FavoriteObjectsMapper.fromResponseMultiple(response))
    );
  }
}
