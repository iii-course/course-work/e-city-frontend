import {Component, OnDestroy} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {AuthService} from "../../../../core/services/auth/auth.service";
import {UserRegistrationData} from "../../../../core/models/user/user-registration-data.model";
import {Subject} from "rxjs";
import {takeUntil} from "rxjs/operators";

export const passwordsMatchValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
  const password = control.get('password');
  const confirmPassword = control.get('confirmPassword');

  return password &&
    confirmPassword &&
    password.value !== confirmPassword.value ?
    { passwordsDoNotMatch: true } : null;
};

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.scss']
})
export class RegistrationFormComponent implements OnDestroy {
  private readonly destroy = new Subject();

  public readonly form = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    firstName: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(100)]),
    lastName: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(100)]),
    password: new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(20)]),
    confirmPassword: new FormControl('', [Validators.required])
  }, { validators: passwordsMatchValidator });

  constructor(
    private readonly router: Router,
    private readonly authService: AuthService
  ) {
  }

  public ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }

  public signUp(): void {
    if (this.form.valid) {
      this.authService.signUp(
        this.getUserRegistrationData() as UserRegistrationData
      ).pipe(
        takeUntil(this.destroy)
      ).subscribe({
        next: () => this.onRegistrationSuccess(),
        error: () => this.onRegistrationError()
      });
    }
  }

  private onRegistrationSuccess(): void {
    alert('You have signed up successfully!');
    this.router.navigateByUrl('auth/login');
  }

  private onRegistrationError(): void {
    alert('Oops... Unexpected error happened. Please try again');
    this.form.reset();
  }

  private getUserRegistrationData(): Partial<UserRegistrationData> {
    return {
      email: this.form.value.email ?? undefined,
      firstName: this.form.value.firstName ?? undefined,
      lastName: this.form.value.lastName ?? undefined,
      password: this.form.value.password ?? undefined,
    };
  }
}
