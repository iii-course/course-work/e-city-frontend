import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../../../core/services/user/user.service";
import {User} from "../../../../core/models/user/user.model";

@Component({
  selector: 'app-settings-page',
  templateUrl: './settings-page.component.html',
  styleUrls: ['./settings-page.component.scss']
})
export class SettingsPageComponent {
  public readonly user: User | undefined;
  public readonly form: FormGroup;

  constructor(userService: UserService) {
    this.user = userService.getUser();
    this.form = new FormGroup({
      email: new FormControl(this.user?.email, [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
    });
  }

  public updateSettings(): void {

  }
}
