
import {MapObject} from "../../../feature/map/models/map-object.model";
import {User} from "../user/user.model";

export interface Review {
  id: string,
  object: MapObject,
  rate: number,
  heading: string,
  text: string,
  author: User
}
