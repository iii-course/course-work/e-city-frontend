import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MapRoutingModule } from './map-routing.module';
import { MapPageComponent } from './pages/map-page/map-page.component';
import {TranslateModule} from "@ngx-translate/core";
import { MapComponent } from './components/map/map.component';
import {MapObjectsService} from "./services/map-objects/map-objects.service";
import { MapObjectDrawerComponent } from './components/map-object-drawer/map-object-drawer.component';
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {MapObjectCardComponent} from "./components/map-object-card/map-object-card.component";
import {MapObjectsListComponent} from "./components/map-objects-list/map-objects-list.component";
import {MatCardModule} from "@angular/material/card";


@NgModule({
  declarations: [
    MapPageComponent,
    MapComponent,
    MapObjectDrawerComponent,
    MapObjectCardComponent,
    MapObjectsListComponent
  ],
    imports: [
        CommonModule,
        MapRoutingModule,
        TranslateModule,
        MatIconModule,
        MatButtonModule,
        MatCardModule
    ],
  exports: [
    MapObjectDrawerComponent,
    MapObjectsListComponent
  ],
  providers: [MapObjectsService]
})
export class MapModule { }
