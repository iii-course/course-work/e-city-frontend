import { Injectable } from '@angular/core';
import {Observable, of} from "rxjs";
import {MapObject} from "../../models/map-object.model";
import { ApiService } from 'src/app/core/services/api/api.service';
import {apiLinks} from "../../../../core/services/api/api-links";
import {MapObjectResponse} from "../../models/map-object.response";
import {MapObjectsMapper} from "../../../../core/mappers/map-objects.mapper";
import {map} from "rxjs/operators";

@Injectable()
export class MapObjectsService {
  constructor(private readonly apiService: ApiService) { }

  public getAllObjects(): Observable<MapObject[]> {
    return this.apiService.get<MapObjectResponse[]>(apiLinks.getAllObjects).pipe(
      map(response => MapObjectsMapper.fromResponseMultiple(response))
    );
  }
}
