import {UserResponse} from "../models/user/user-response.model";
import {User} from "../models/user/user.model";

export class UsersMapper {
  public static fromResponse(userResponse: UserResponse): User {
    return { ...userResponse, id: userResponse.id.toString() };
  }
}
