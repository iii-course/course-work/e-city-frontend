import {Component, Inject,} from '@angular/core';
import {DRAWER_DATA} from "../../../../core/tokens/drawer-data.token";
import {MapObject} from "../../models/map-object.model";
import {MatDrawer} from "@angular/material/sidenav";
import {DrawerService} from "../../../../core/services/drawer/drawer.service";

@Component({
  selector: 'app-map-object-drawer',
  templateUrl: './map-object-drawer.component.html',
  styleUrls: ['./map-object-drawer.component.scss']
})
export class MapObjectDrawerComponent {
  constructor(
    @Inject(DRAWER_DATA) public mapObject: MapObject,
    private readonly drawerService: DrawerService
  ) { }

  public closeDrawer(): void {
    this.drawerService.closeDrawer();
  }

}
