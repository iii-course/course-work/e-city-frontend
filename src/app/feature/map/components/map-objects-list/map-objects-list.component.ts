import {Component, Input} from '@angular/core';
import {MapObject} from "../../models/map-object.model";

@Component({
  selector: 'app-map-objects-list[objects]',
  templateUrl: './map-objects-list.component.html',
  styleUrls: ['./map-objects-list.component.scss']
})
export class MapObjectsListComponent {
  @Input() public objects!: MapObject[];
}
