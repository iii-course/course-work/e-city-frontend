import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {Review} from "../../../../core/models/review/review.model";

@Component({
  selector: 'app-review-card[review]',
  templateUrl: './review-card.component.html',
  styleUrls: ['./review-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ReviewCardComponent {
  @Input() public review!: Review;

  constructor() { }

  public getAuthorImage(): string {
    return this.review.author.imageUrl ?? './assets/images/profile_image.webp';
  }

}
