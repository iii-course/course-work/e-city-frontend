import {AfterViewInit, Component, OnDestroy} from '@angular/core';
import * as L from "leaflet";
import {MapObjectsService} from "../../services/map-objects/map-objects.service";
import {Subscription} from "rxjs";
import {MapObject} from "../../models/map-object.model";
import {DrawerService} from "../../../../core/services/drawer/drawer.service";
import {MapObjectDrawerComponent} from "../map-object-drawer/map-object-drawer.component";

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements AfterViewInit, OnDestroy {
  private readonly subscription = new Subscription();

  constructor(
    private readonly mapObjectsService: MapObjectsService,
    private readonly drawerService: DrawerService
  ) { }

  public ngAfterViewInit(): void {
    const map = L.map('map').setView([51.505, -0.09], 13);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    // L.marker([51.5, -0.09]).addTo(map)
    //   .bindPopup('A pretty CSS3 popup.<br> Easily customizable.')
    //   .openPopup();

    this.subscription.add(
      this.mapObjectsService.getAllObjects().subscribe(objects => {
        objects.forEach(object => {
          const {latitude, longitude} = object.coordinates;
          const marker = L.marker([latitude, longitude]);

          marker.on('click', () => this.objectSelected(object));

          marker.addTo(map);
        });
      })
    );
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  private objectSelected(object: MapObject): void {
    this.drawerService.openDrawer(MapObjectDrawerComponent, object);
    console.log(object);
  }
}
