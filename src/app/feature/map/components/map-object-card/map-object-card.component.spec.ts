import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapObjectCardComponent } from './map-object-card.component';

describe('MapObjectCardComponent', () => {
  let component: MapObjectCardComponent;
  let fixture: ComponentFixture<MapObjectCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapObjectCardComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MapObjectCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
