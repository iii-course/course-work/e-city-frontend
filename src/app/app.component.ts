import {
  AfterContentInit,
  Component,
  Injector,
  OnDestroy,
  Type,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {DrawerService} from "./core/services/drawer/drawer.service";
import {DRAWER_DATA} from "./core/tokens/drawer-data.token";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterContentInit {
  @ViewChild("drawerContainer", { read: ViewContainerRef }) public drawerContainer: ViewContainerRef | undefined;

  public isDrawerOpened = false;

  constructor(
    translate: TranslateService,
    private readonly drawerService: DrawerService
  ) {
    translate.setDefaultLang('en');
    translate.use('en');
  }

  public ngAfterContentInit(): void {
    this.drawerService.drawer$.subscribe(drawer => {
      if (drawer) {
        this.addDrawer(drawer.component, drawer.data);
      }
      this.isDrawerOpened = !!drawer;
    });
  }

  public addDrawer(component: Type<any>, data?: any): void {
    this.drawerContainer?.clear();
    this.drawerContainer?.createComponent(component, {
      injector: Injector.create({
        providers: [
          {
            provide: DRAWER_DATA, useValue: data
          }
        ]
      })
    });
  }

  public closeDrawer(): void {
    //this.isDrawerOpened = false;
    this.drawerService.closeDrawer();
  }
}
