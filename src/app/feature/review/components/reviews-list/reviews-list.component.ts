import {Component, Input} from '@angular/core';
import {Review} from "../../../../core/models/review/review.model";

@Component({
  selector: 'app-reviews-list[reviews]',
  templateUrl: './reviews-list.component.html',
  styleUrls: ['./reviews-list.component.scss']
})
export class ReviewsListComponent {
  @Input() public reviews!: Review[];
}
