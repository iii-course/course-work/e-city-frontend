export interface UserLoginData {
  readonly email: string,
  readonly password: string
}
