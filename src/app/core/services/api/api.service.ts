import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../../environments/environment";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private readonly http: HttpClient) { }

  public get<T>(url: string): Observable<T> {
    return this.http.get<T>(this.withBaseUrl(url));
  }

  public post<T>(url: string, data: any): Observable<T> {
    return this.http.post<T>(this.withBaseUrl(url), data);
  }

  private withBaseUrl(path: string): string {
    return environment.baseUrl + path;
  }
}
